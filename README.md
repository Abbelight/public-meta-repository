# Public meta repository

This repository is an indirection to the private Abbelight `meta-repository`.

To use it copy the file [cmake/meta.cmake](./cmake/meta.cmake) in the repository where it should be used.
