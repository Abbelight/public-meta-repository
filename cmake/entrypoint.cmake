# This entry point handle access to private meta repository

# Detect access to GitLab and provide:
#
# * ABL_CI_BUILD to tell whether or not the build is done on the CI
# * ABL_GIT_CREDENTIALS to provide the credentials to use to access to Abbelight git repository
if(DEFINED ENV{CI_JOB_TOKEN})
    message(STATUS "Meta (Public): CI build detected, providing git credential")
    set(ABL_CI_BUILD
        TRUE
        CACHE INTERNAL "")
    set(ABL_GIT_CREDENTIALS
        "gitlab-ci-token:$ENV{CI_JOB_TOKEN}@"
        CACHE INTERNAL "")
else()

    set(ABL_CI_BUILD
        FALSE
        CACHE INTERNAL "")
    message(STATUS "Meta (Public): Local build detected")

    if(DEFINED ENV{ABL_GITLAB_TOKEN})
        message(STATUS "Meta (Public): Credential detect using oauth2")
        set(ABL_GIT_CREDENTIALS
            "oauth2:$ENV{ABL_GITLAB_TOKEN}@"
            CACHE INTERNAL "")
    else()
        message(STATUS "Meta (Public): No credential provided, using default")
        set(ABL_GIT_CREDENTIALS
            ""
            CACHE INTERNAL "")
    endif()
endif()

include(FetchContent)

if(DEFINED ABL_TAG_META)
    set(META_TAG ${ABL_TAG_META})
    message(STATUS "Meta (Public): tag provided, using ${META_TAG}")
else()
    set(META_TAG main)
    message(STATUS "Meta (Public): tag not provided, using main")
endif()

# Fetch meta with the desired access
FetchContent_Declare(
    MetaRepository
    GIT_REPOSITORY "https://${ABL_GIT_CREDENTIALS}gitlab.com/Abbelight/meta-repository.git"
    GIT_TAG ${META_TAG}
    TIMEOUT 60)

FetchContent_MakeAvailable(MetaRepository)

message(STATUS "Meta (Public): loading entrypoint.cmake")
include(${metarepository_SOURCE_DIR}/cmake/entrypoint.cmake)
message(STATUS "Meta (Public): loaded")
