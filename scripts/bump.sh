#! /usr/bin/env bash

if [ $# -lt 3 ] ; then
    echo "Usage: $0 path/to/model.cmake.in http://repository branch [output]"
    exit -1
fi

INPUT_FILE=$(readlink -f "$1")
OUTPUT_FILE=${4:-"$(dirname "${INPUT_FILE}")/$(basename "${INPUT_FILE}" .in)"}
REPOSITORY=$2
BRANCH=$3

echo "Getting ${REPOSITORY} SHA-1"

SHA_1=$(git ls-remote --quiet "${REPOSITORY}" "refs/heads/${BRANCH}" | cut -f1)

if [[ "${SHA_1}" =~ [A-Fa-f0-9]{40} ]] ; then
    echo "${REPOSITORY} SHA-1: ${SHA_1}"
else
    echo "${REPOSITORY} SHA-1 not found. Returned string: ${SHA_1}"
    exit -1
fi

echo "Updating ${OUTPUT_FILE}"
sed -e "s/%SHA-1%/${SHA_1}/g" "${INPUT_FILE}" > "${OUTPUT_FILE}"
echo "File updated"
